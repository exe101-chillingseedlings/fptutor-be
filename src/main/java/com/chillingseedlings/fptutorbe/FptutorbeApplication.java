package com.chillingseedlings.fptutorbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FptutorbeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FptutorbeApplication.class, args);
	}

}
